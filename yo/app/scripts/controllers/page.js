'use strict';

angular.module('yoApp')
  .controller('page', function ($scope, $stateParams, $rootScope, $translate, $filter) {
  	var t = function(j){ return $filter('translate')(j); }   
    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      $rootScope.title = sprintf("%s - %s", t('global.title'), t('menu.' + $stateParams.pageSlug));
      $rootScope.customCss = sprintf("styles/%s.css", $stateParams.device);
    });
    
    $rootScope.device = $stateParams.device;
    
    $rootScope.lang = $rootScope.setCurrentLanguage($stateParams.language);
    $translate.use($rootScope.lang);
    var pageSlug = sprintf("%s", $stateParams.pageSlug);
    
    $rootScope.slug = pageSlug;
    
    
    
    console.log("in page controller. lang = " + $rootScope.lang);


	if (pageSlug == "portfolio"){
	  $scope.pageSlug = pageSlug;
	  $scope.items = [
        { link : "work1", name : "Работа 1" },
        { link : "work2", name : "Работа 2" },
        { link : "work3", name : "Работа 3" }
      ];
    }
    
  });
