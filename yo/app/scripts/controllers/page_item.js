console.log("page_item.js");

'use strict';

angular.module('yoApp')
  .controller('page_item', function ($scope, $stateParams, $rootScope, $translate, $filter) {
 
  	var t = function(j){ return $filter('translate')(j); }  
    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      $rootScope.title = sprintf("%s - %s - %s", t('global.title'), t('menu.' + $stateParams.pageSlug), $stateParams.itemSlug);
    });
  
    $rootScope.lang = $rootScope.setCurrentLanguage($stateParams.language);
    $translate.use($rootScope.lang);
    console.log("in page_item controller");

    
    var pageSlug = sprintf("%s", $stateParams.pageSlug);
    var itemSlug = sprintf("%s", $stateParams.itemSlug);

	if (pageSlug == "portfolio"){
	  $scope.pageSlug = pageSlug;
	  $scope.itemId = itemSlug;

	  $scope.content = itemSlug; 
    }
  });
