'use strict';

var yoApp = angular.module('yoApp', [
	'ui.router',
	'pascalprecht.translate'
]);

yoApp.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  
  $urlRouterProvider.otherwise("/ru/home");
  
  //
  // Now set up the states 
  
  
  var userAgent = window.navigator.userAgent.toLowerCase(),
    orient,
    iphone = /iphone/.test(userAgent),
    ipad = /ipad/.test(userAgent),
/*     iphone = true, */ 
/*     ipad = true, */
/*     kindle = /Kindle|Silk|KFTT|KFOT|KFJWA|KFJWI|KFSOWI|KFTHWA|KFTHWI|KFAPWA|KFAPWI/i.test(userAgent); */
    kindle = /kindle fire/i.test(userAgent);
    if(window.innerHeight > window.innerWidth){
      orient = 0;
    } else {
        orient = 90;
    }
    console.log('orient: '+orient+'\nuserAgent: '+userAgent+'\nipad: '+ipad+'\niphone: '+iphone+'\nkindle: '+kindle);

  var getDevice = function(){ return (iphone ? "iphone" : (ipad ? (Math.abs(orient) === 90 ? "ipad-landscape" : "ipad-portrait") : (kindle ? "kindle" : "normal"))); };
  var getDetectedView = function(mask, pageSlug){ return sprintf(mask, getDevice(), pageSlug); };
  
  $stateProvider
    .state("page", {
      url: '{sl:[\/]{0,1}}{language:[a-z]{0,2}}/:pageSlug',
      controller: "page",
      templateUrl: function (stateParams){ stateParams.device = getDevice(); return getDetectedView("views/%s/%s.html", stateParams.pageSlug); }
    })
    .state('page.item', {
      url: '/:itemSlug',
      controller: "page_item",
      templateUrl: function (stateParams){ return getDetectedView("views/%s/%s.item.html", stateParams.pageSlug); }
     
    })
    .state('page.item.more', {
      url: "/more",
      controller: "page_item_more",
      templateUrl: function (stateParams){ return getDetectedView("views/%s/%s.item.more.html", stateParams.pageSlug); }
    })
});

yoApp.config(['$translateProvider', function ($translateProvider) {
  
  $translateProvider.useStaticFilesLoader({
    prefix: 'static/locale-',
    suffix: '.json'
  });

  $translateProvider.preferredLanguage('ru');
  
}]);


yoApp.run(function ($rootScope) {
  $rootScope.languages = ['ru', 'en'];
  $rootScope.defaultLanguage = $rootScope.languages[0];
  $rootScope.lang = $rootScope.languages[0];
  $rootScope.setCurrentLanguage = function(lang){ return ($rootScope.languages.indexOf(lang) != -1) ? lang : $rootScope.defaultLanguage; }
  $rootScope.slug = null;
  
});
