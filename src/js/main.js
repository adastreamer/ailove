;'use strict';

var parallax;

parallax = {
	init : function() {
		this.container      = document.querySelector( '#container' );
		this.hash 			= '';
		this.clickNext;
		this.home           = this.container.querySelector( '#home' );
		this.about          = this.container.querySelector( '#aboutus' );
		this.moveable       = this.container.querySelector( '#moveable' );
		this.contacts       = this.container.querySelector( '#contact' );
		this.whatwedo       = this.container.querySelector( '#wedo' );
		this.portfolio      = this.container.querySelector( '#portf' );
		this.show 			= this.container.querySelector( '#work' );

		this.listContainer  = this.portfolio.querySelector( '.listcontainer' );
		this.scrollbar      = this.portfolio.querySelector( '.scrollbar > span' );
		this.videoContainer = this.home.querySelector( '.video' );
		this.letter         = this.about.querySelectorAll( '.animated-letter' );
		this.letterLen      = this.letter.length;
		this.canAnimateLetter = true;
		this.dragScrollbar  = false;
		this.scrollPosition = 0;
		this.nextButton 	= this.container.querySelector( 'a.nextWork' );
		this.prevButton 	= this.container.querySelector( 'a.prevWork' );
		this.playButton = this.home.querySelector( '#showreel span ' );
		this.mouseClick = document.createEvent('MouseEvents');
		this.mouseClick.initMouseEvent( 'click', true, true, window, 1, 12, 345, 7, 220, false, false, true, false, 0, null );


		this.container.onmousemove = function( event ) { parallax.handlerMove( event ) };
		window.onresize = function() { parallax.handlerResize() };
		window.addEventListener( 'hashchange', function() { parallax.moveScreen(); } );
		window.addEventListener( 'load', parallax.moveScreen() );
		this.portfolio.onwheel = function( event ) { parallax.handlerWeel( event ) };
		this.scrollbar.onmousedown = function( event ) { parallax.initDrag( event ) };
		this.portfolio.onmouseup = function() { parallax.endDrag() };
		this.playButton.onclick = function() { parallax.playVideo() };
		this.videoContainer.onclick = function( event ) { parallax.stopVideo( event ) };

		this.moveable.addEventListener( 'transitionend', function (event) { parallax.animateLetter(event) });

		this.handlerResize();
	},

	initDrag : function ( e ) {
		this.dragScrollbar = true;
		this.elem0 = parseInt(this.scrollbar.style.marginLeft) || 0;
		this.X0 = e.pageX;
		this.scrollbar.style.webkitTransitionDuration = this.scrollbar.style.transitionDuration = 
		this.listContainer.style.webkitTransitionDuration = this.listContainer.style.transitionDuration = '0s';
	},

	endDrag : function () {
		this.dragScrollbar = false;
		this.scrollbar.style.webkitTransitionDuration = this.scrollbar.style.transitionDuration = 
		this.listContainer.style.webkitTransitionDuration = this.listContainer.style.transitionDuration = '';
	},

	handlerMove : function( e ) {
		var positionX        = e.pageX,
			positionY        = e.pageY,
			documentX        = window.innerWidth,
			documentY        = window.innerHeight,
			clockwise        = this[ this.hash ].querySelector( '.clockwise' ),
			counterclockwise = this[ this.hash ].querySelector( '.counterclockwise' );

		switch ( this.hash ) {
			case 'home' :
				var showreel = this[ this.hash ].querySelector( '#showreel img' );

				showreel.style.marginTop  = - positionY / 50 + 'px';
				showreel.style.marginLeft = - positionX / 50 + 'px';

				counterclockwise.querySelector( '.line_1' ).style.marginTop    =   positionY / 2 - documentY / 4 + 'px';
				counterclockwise.querySelector( '.line_2' ).style.marginBottom = - positionY / 3 + documentY / 10 + 'px';
				counterclockwise.querySelector( '.line_3' ).style.marginBottom = - positionY / 20 + documentY / 10 + 'px';
				counterclockwise.querySelector( '.line_4' ).style.marginTop    = - positionY / 8 + documentY / 8 + 'px';
				counterclockwise.querySelector( '.line_5' ).style.marginBottom =   positionY - documentY / 2 + 'px';
				counterclockwise.querySelector( '.line_6' ).style.marginBottom = - positionY / 40 + documentY / 20 + 'px';

				clockwise.querySelector( '.line_1' ).style.marginTop           = - positionX / 2 + documentX / 4 + 'px';
				clockwise.querySelector( '.line_2' ).style.marginTop           =   positionX / 20 - documentX / 40 + 'px';
				clockwise.querySelector( '.line_3' ).style.marginTop           = - positionX / 40 + documentX / 80 + 'px';
				clockwise.querySelector( '.line_4' ).style.marginTop           =   positionX / 5 - documentX / 10 + 'px';
				clockwise.querySelector( '.line_5' ).style.marginTop           = - positionX / 8 + documentX / 16 + 'px';
				clockwise.querySelector( '.line_6' ).style.marginBottom        =   positionX / 3 - documentX / 6 + 'px';
				clockwise.querySelector( '.line_7' ).style.marginBottom        = - positionX / 8 + documentX / 16 + 'px';
				clockwise.querySelector( '.line_8' ).style.marginBottom        = - positionX / 80 + documentX / 160 + 'px';
				clockwise.querySelector( '.line_9' ).style.marginBottom        = - positionX / 20 + documentX / 40 + 'px';
			break;

			case 'about' :
				var all  = this[ this.hash ].querySelector( '.all' ),
					is   = this[ this.hash ].querySelector( '.is' ),
					full = this[ this.hash ].querySelector( '.full' ),
					of   = this[ this.hash ].querySelector( '.of' ),
					love = this[ this.hash ].querySelector( '.love' );

				if (this.canAnimateLetter) {
					all.querySelector( '.a b' ).style.width    = positionY / documentY * 100 + '%';
					all.querySelector( '.l b' ).style.height   = 30 + positionX / documentX * 70 + '%';
					all.querySelector( '.l2 b' ).style.height  = 70 - positionY / documentY * 30 + '%';

					is.querySelector( '.i b' ).style.height    = positionX / documentX * 90 + '%';
					is.querySelector( '.s b' ).style.height    = 100 - positionX / documentX * 100 + '%';

					full.querySelector( '.f b' ).style.width   = 10 + positionX / documentX * 70 + '%';
					full.querySelector( '.u b' ).style.width   = 100 - positionY / documentY * 100 + '%';
					full.querySelector( '.l b' ).style.height  = positionX / documentX * 100 + '%';
					full.querySelector( '.l2 b' ).style.height = 100 - positionX / documentX * 100 + '%';

					of.querySelector( '.o b' ).style.height    = 100 - positionX / documentX * 100 + '%';
					of.querySelector( '.f b' ).style.width     = 70 - positionY / documentY * 70 + '%';

					love.querySelector( '.l b' ).style.height  = 80 - positionX / documentX * 70 + '%';
					love.querySelector( '.o b' ).style.width   = positionY / documentY * 70 + '%';
					love.querySelector( '.v b' ).style.width   = positionX / documentX * 50 + '%';
					love.querySelector( '.e b' ).style.height  = positionY / documentY * 70 + '%';
				}

				this[ this.hash ].querySelector( '.people' ).style.marginLeft = documentX / 60 - positionX / 60 + 'px';
				this[ this.hash ].querySelector( '.projects' ).style.marginRight = positionY / 30 + 'px';

				clockwise.querySelector( '.line_1' ).style.marginTop = - positionX / 2 + documentX / 4 + 'px';
				clockwise.querySelector( '.line_2' ).style.marginBottom = - positionX / 20  + 'px';
				clockwise.querySelector( '.c1' ).style.marginTop = - positionX / 60  + 'px';
				clockwise.querySelector( '.line_3' ).style.marginTop = positionY / 20  + 'px';
				clockwise.querySelector( '.line_4' ).style.marginTop = - documentY / 8 + positionY / 10  + 'px';
				clockwise.querySelector( '.line_5' ).style.marginTop = - positionY / 40  + 'px';
				clockwise.querySelector( '.c2' ).style.marginTop = - positionX / 20  + 'px';
				clockwise.querySelector( '.line_6' ).style.marginTop = - positionY / 6  + 'px';
				clockwise.querySelector( '.line_7' ).style.marginTop =  documentX / 15 - positionX / 10  + 'px';
				clockwise.querySelector( '.line_8' ).style.marginLeft =  documentY / 15 - positionY / 7  + 'px';
			break;

			case 'contacts' :
				this[ this.hash ].querySelector( 'p' ).style.marginLeft = - positionX / 20 + 'px';
				this[ this.hash ].querySelector( 'p' ).style.marginTop = - positionY / 20 + 'px';

				this[ this.hash ].querySelector( '.c1' ).style.marginTop = positionY / 100 + 'px';
				this[ this.hash ].querySelector( '.c1' ).style.marginRight = positionX / 100 + 'px';
				this[ this.hash ].querySelector( '.c1 > .triangle' ).style.marginLeft = positionX / 50 + 'px';
				this[ this.hash ].querySelector( '.c1 > .triangle' ).style.marginTop = positionY / 50 + 'px';
				this[ this.hash ].querySelector( '.c1 > .triangle:nth-of-type(2)' ).style.marginLeft = positionX / 100 + 'px';
				this[ this.hash ].querySelector( '.c1 > .triangle:nth-of-type(2)' ).style.marginTop = positionY / 100 + 'px';

				clockwise.style.marginTop= - positionY / 40 + documentY / 40 + 'px';
				clockwise.style.marginLeft= - positionX / 40 + documentX / 40 + 'px';
				counterclockwise.style.marginTop = - positionY / 40 + documentY / 40 + 'px';
				counterclockwise.style.marginLeft = - positionX / 40 + documentX / 40 + 'px';

				clockwise.querySelector( '.line_1' ).style.marginLeft = - positionX / 40 + documentX / 80 + 'px';
				clockwise.querySelector( '.line_2' ).style.marginLeft = positionX / 40 - documentX / 80 + 'px';

				counterclockwise.querySelector( '.line_1' ).style.marginLeft = - positionY / 40 + documentY / 80 + 'px';
				counterclockwise.querySelector( '.line_2' ).style.marginLeft = positionY / 40 - documentY / 80 + 'px';
			break;

			case 'whatwedo' :
				var lineTop = clockwise.querySelector( '.line_top' ),
					lineBottom = clockwise.querySelector( '.line_bottom' );

				this[ this.hash ].querySelector( '.c1' ).style.marginTop = positionY / 50 + 'px';
				this[ this.hash ].querySelector( '.c1' ).style.marginLeft = positionX / 100 + 'px';

				this[ this.hash ].querySelector( '.c2' ).style.marginLeft = - positionY / 60 + documentY / 60 + 'px';
				this[ this.hash ].querySelector( '.c2' ).style.marginTop = - positionX / 40 + documentY / 40 + 'px';

				clockwise.querySelector( '.line_1' ).style.marginTop = - positionX / 100 + 'px';
				clockwise.querySelector( '.line_2' ).style.marginTop = positionY / 50 + 'px';
				clockwise.querySelector( '.line_3' ).style.marginTop = - positionX / 20 + 'px';

				lineTop.style.marginTop = positionY / 6 + 'px';
				lineBottom.style.marginTop = positionX / 20 + 'px';

				if ( e.target.tagName === 'LI' ) {
					e.target.onmousemove = function() {
						clockwise.classList.add( this.className );
						lineTop.style.left = this.offsetTop - this.clientHeight * 0.1 + 'px';
						lineBottom.style.left = this.offsetTop + this.clientHeight * 1.1 + 'px';
					};

					e.target.onmouseleave = function() {
						clockwise.classList.remove( this.className );
						lineTop.style.left = lineBottom.style.left = '';
					};
				}
			break;

			case 'portfolio' :
				var line_under = counterclockwise.querySelector( '.line_under' );

				this[ this.hash ].querySelector( '.c1' ).style.marginTop = positionY / 50 + 'px';
				this[ this.hash ].querySelector( '.c1' ).style.marginLeft = -positionX / 50 + 'px';

				clockwise.querySelector( '.line_1' ).style.marginTop = - positionX / 40 + 'px';
				clockwise.querySelector( '.line_2' ).style.marginTop = positionY / 20 + 'px';
				clockwise.querySelector( '.line_4' ).style.marginTop = - positionY / 10 + 'px';
				clockwise.querySelector( '.line_5' ).style.marginTop = positionX / 80 + 'px';


				line_under.style.marginLeft = - positionX / 50 + 'px';

				if ( e.target.parentNode.tagName === 'LI' ) {
					var ul = e.target.parentNode.parentNode;
					e.target.onmousemove = function() {
						line_under.style.top = this.offsetTop + this.clientHeight / 2 + 'px';
						line_under.style.left = ul.offsetLeft - this.clientWidth * 0.1 + 'px';
						line_under.style.backgroundColor = 'rgba(0,51,255,1)';
						line_under.style.width = this.clientWidth * 1.1 + 'px';
					};

					e.target.onmouseleave = function() {
						line_under.style.backgroundColor = '';
						line_under.style.top = '';
						line_under.style.left = '';
						line_under.style.width = '';
					};
				}

				if ( this.dragScrollbar ) {
					var dragPosition = this.elem0 + positionX - this.X0,
							overscroll = this.scrollbar.parentNode.clientWidth - this.scrollbar.clientWidth;
					dragPosition < 0 
					? dragPosition = 0 
					: dragPosition > overscroll
					? dragPosition = overscroll
					: dragPosition;

					this.scrollPosition = - dragPosition / overscroll * ( this.listContainer.clientWidth - window.innerHeight * 1.3 );
					this.scrollbar.style.marginLeft = dragPosition + 'px';
					this.handlerWeel();
				}
			break;

			case 'show' :
				clockwise.querySelector( '.line_2' ).style.marginTop = - positionX / 40 + 'px';
				clockwise.querySelector( '.line_3' ).style.marginTop = positionY / 60 + 'px';

				counterclockwise.querySelector( '.line_1' ).style.marginTop = - positionY / 90 + 'px';

				var workContainer = this.show.querySelector( '.viewed' ) || undefined;

				if ( workContainer ) {
					workContainer.querySelector( '.c1 > img' ).style.marginTop = - positionY / 100 + 'px';
					workContainer.querySelector( '.c1 > img' ).style.marginLeft = - positionX / 100 + 'px';

					workContainer.querySelector( '.info' ).style.marginTop = positionX / 70 + 'px';
					workContainer.querySelector( '.info' ).style.marginLeft = positionY / 70 + 'px';

					workContainer.querySelector( '.more' ).style.marginBottom = positionY / 70 + 'px';
					workContainer.querySelector( '.more' ).style.marginLeft = positionX / 70 + 'px';
				}
			break;
		}
	},

	handlerResize : function() {
		var windowWidth = window.innerWidth,
			fontSize    = windowWidth / 2000 > 1 ? 1 : windowWidth / 2000 < 0.7 ? 0.7 : windowWidth / 2000;
		this.container.style.fontSize = fontSize + 'em';
	},

	handlerWeel : function( e ) {
		var delta,
			overscroll = this.listContainer.clientWidth - window.innerHeight * 1.3;
		e = e || window.event;

		if ( e ) {
			delta = e.deltaY || e.detail || e.wheelDelta;

			if ( delta > 0 ) {
				this.scrollPosition < 0 ? this.scrollPosition += 100 : this.scrollPosition = 0;
			} else {
				this.scrollPosition > - overscroll ? this.scrollPosition -= 100 : this.scrollPosition = - overscroll;
			}

			this.scrollbar.style.marginLeft = - this.scrollPosition / ( overscroll ) * ( this.scrollbar.parentNode.clientWidth - this.scrollbar.clientWidth ) + 'px';
		}

		this.listContainer.style.marginLeft = this.scrollPosition + 'px';
	},

	animateLetter: function ( event ) {
		if ( event.target.id === 'moveable') {
			
			if ( this.hash === 'about' ) {
				this.canAnimateLetter = false;
			
				for (var i = 0; i < this.letterLen; i++) {
					(function ( letter, duration, i ) {
						setTimeout(function () {
							letter.remove( 'rotated-letter' );

							if (i === parallax.letterLen - 1) {
								for (var j = 0; j < parallax.letterLen; j++) {
									(function ( item, j ) {
										setTimeout(function () {
											item.remove( 'animated-letter' );

											if (j === parallax.letterLen - 1) {
												parallax.canAnimateLetter = true;

												//$(parallax.container).trigger('mousemove');
											}
										}, 1000);
									})(parallax.letter[j].classList, j);
								}
							}

						}, duration);
					})( this.letter[i].classList, i * 75, i )
				}
			} else {
				for (var i = 0; i < this.letterLen; i++) {
					this.letter[i].classList.add( 'animated-letter', 'rotated-letter' );
				}
			}
		}
	},

	moveScreen : function() {
		var activeLink 	= this.container.querySelector( '.active' ),
			buttons 	= this.container.querySelector( '.buttons' );
	
		this.listContainer.style.marginLeft = '';

		if ( location.hash.slice( 1, 5 ) != 'show' ) {
			this.hash = location.hash.slice( 1 ) || 'home';
		} else {
			this.hash = location.hash.slice( 1, 5 );
		}

		if ( activeLink ) activeLink.classList.remove( 'active' );
		this.container.classList.remove( 'hidelinks' );
		buttons.style.top = '';

		switch ( this.hash ) {
			case 'home' :
				this.moveable.style.top = '';
				this.moveable.style.left = '';
			break;

			case 'about' :
				this.moveable.style.top = '205.885%';
				this.moveable.style.left = '90%';
				this.container.querySelector( '.' + this.hash ).classList.add( 'active' );
			break;

			case 'contacts' :
				this.moveable.style.top  = '-205.885%';
				this.moveable.style.left = '-90%';
				this.container.querySelector( '.' + this.hash ).classList.add( 'active' );
			break;

			case 'whatwedo' :
				this.moveable.style.top  = '-140%';
				this.moveable.style.left = '205.885%';
				this.container.querySelector( '.' + this.hash ).classList.add( 'active' );
			break;

			case 'portfolio' :
				this.moveable.style.top = '140%';
				this.moveable.style.left = '-205.885%';
				this.listContainer.style.marginLeft = this.scrollPosition + 'px';
				this.container.querySelector( '.' + this.hash ).classList.add( 'active' );
			break;

			case 'show' :
				this.moveable.style.top = '-205.885%';
				this.moveable.style.left = '-345.885%';
				this.container.querySelector( '.portfolio' ).classList.add( 'active' );
				buttons.style.top = 0;

				this.createWorkContainer( location.hash.slice(6) );
			break;
		}
	},

	createWorkContainer : function( workName ) {
		var workContainer 	= this.show.querySelector( '.work_' + workName ) || undefined,
			viewed 			= this.show.querySelector( '.viewed' ) || undefined,
			full 			= this.show.querySelector( '.full' ) || undefined,
			links 			= [].slice.call( this.listContainer.getElementsByTagName( 'A' ) ),
			IN 				= this.clickNext ? 'nextWorkIn' : 'prevWorkIn',
			OUT 			= this.clickNext ? 'nextWorkOut' : 'prevWorkOut',
			delay 			= 790,
			fullDelay 		= full ? 1000 : 0;

		if ( viewed ) {
			setTimeout( function() {
				viewed.classList.remove( 'viewed' );
				viewed.classList.add( OUT );

				setTimeout( function() {
					viewed.classList.remove( OUT );
				}, delay );

			}, fullDelay );
		}

		if ( full ) {
			full.classList.remove( 'full' );
		}


		if( !workContainer ) {
			workContainer = document.createElement( 'div' );
			workContainer.classList.add( 'work_' + workName );
			this.show.appendChild( workContainer );

			$.ajax({
				url: 'ajax/' + workName + '/index.html',
				cache: false,
				success: function(html) {
					$( '.work_' + workName ).html(html);
				}
			});
		} 

		setTimeout( function() {
			workContainer.classList.add( 'viewed' );
			workContainer.classList.add( IN );
			this.container.classList.remove( 'hidelinks' );
			setTimeout( function() {
				workContainer.classList.remove( IN );
			}, delay );
		}, fullDelay );

		links.forEach( function( link, index ) {
			if ( link.hash === location.hash ) {
			var prev = index - 1 >= 0 ? index - 1 : links.length - 1,
					next = index + 1 < links.length - 1 ? index + 1 : 0;

			parallax.prevButton.hash = links[ prev ].hash;
			parallax.nextButton.hash = links[ next ].hash;
			}
		} );


		this.nextButton.onclick = function() {
			parallax.clickNext = true;
		};

		this.prevButton.onclick = function() {
			parallax.clickNext = false;
		};
	},
	playVideo : function() {
		this.videoContainer.style.zIndex = 100000;
		this.videoContainer.style.opacity = 1;
		this.play = this.container.querySelector( '#playerXfMYNNRGtxMX_display_button' );

		this.play.dispatchEvent( this.mouseClick );
	},

	stopVideo : function( e ) {
		if ( e.target.className === 'video' ) {
			this.videoContainer.style.zIndex = '';
			this.videoContainer.style.opacity = '';
			this.play.dispatchEvent( this.mouseClick );
		}
	}
};

parallax.init();
jwplayer.key = "V7wG/YbWS/U54n7wegDuG1EWvDIO3faf7NvSeA==";

jwplayer('playerXfMYNNRGtxMX').setup({file: 'http://dl.dropboxusercontent.com/u/3902787/video/ailove.mp4', image: 'http://www.longtailvideo.com/content/images/jw-player/lWMJeVvV-876.jpg'});

/*
 *  Да
 *
 * 
 jwplayer('playerXfMYNNRGtxMX').setup({file: 'video/ailove.mp4',image: 'http://www.longtailvideo.com/content/images/jw-player/lWMJeVvV-876.jpg'});
 */
 function triangle(text) {
	var lastBr,
		replace,
		numberLine = 1,
		numberChar = 30,
		triangleText = [].slice.call( text );

	triangleText.forEach( function(chr, index) {
		if ( chr === ' ') {
			if ( index >= numberChar ) {
				replace = index - 3 > numberChar ? lastBr : index;
				triangleText[ replace ] = '<br>';
				numberChar = replace + 30 + numberLine * 3;
				numberLine ++;
			}
			lastBr = index;
		}
	});
	return triangleText.join("");
}
function strip_tags( str ){
    return str.replace(/<\/?[^>]+>/gi, '');
}
function stepCaseOne(workName, dataArticle){
	var dfd = new jQuery.Deferred();
	$.ajax(
	{
		url: 'ajax/' + workName + '/full.html',
		cache: false,
		success: function(html)
		{
			$('.work_' + workName + ' > .fullview').html(html);
			dfd.resolve( workName, dataArticle );
		}
	});
	return dfd.promise();
}
function stepCaseTo(workName, data){
	var workContainer = parallax.show.querySelector('.work_' + workName);
			if(data.data.object.media){
				$('.fullview .img > img').replaceWith('<video poster="'+"http://dev.newailove.dev.ailove.ru"+data.data.object.main_image+'" loop="loop" controls="controls" tabindex="0"><source src="http://dl.dropboxusercontent.com/u/3902787/video/ailove.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\' /></video>');
/* 				jwplayer('playerPzPbbtKhnewG').setup({file: "http://dev.newailove.dev.ailove.ru"+data.data.object.media,image: }); '++'*/
			} else {
				workContainer.querySelector('.fullview .img > img').src = "http://dev.newailove.dev.ailove.ru"+data.data.object.main_image;
			}
			$('p[dataObjectClient]').html( data.data.object.client );
			$('p[dataObjectPurpose]').html( data.data.object.purpose );
			$('p[dataObjectIdea]').html( data.data.object.idea );
			$('div[dataObjectImplementation]').html( data.data.object.implementation );
			$('i[dataObjectInsight]').html( data.data.object.insight );
			$('article > .slider').html( "" );
			var cont = $('<div dataObjectImages></div>');
			_.each(data.data.object.images, function(element, index, list){
				$('<input type="radio" name="slider" id="img_'+index+'"><label for="img_'+index+'"><span></span><hr></label>').appendTo('article > .slider');
				cont.append('<div class="imgcontainer"><img src="'+"http://dev.newailove.dev.ailove.ru"+element.image+'" height="755" width="989"></div>').appendTo('div[dataObjectImages]');
			});
			$('article > .slider').append(cont);
			$('article > .slider > input:first-child').click();
			$('article > .info > .result').html( '' );
			_.each(data.data.object.results, function(element, index, list){
				$('article > .info > .result').append('<p><span>'+element.number+'</span> '+element.text+'</p>');
			});
			if(data.data.object.awards.length == 0) $('article > .text > .awards').hide();
			$('ul[dataObjectTeam]').html( '' );
			_.each(data.data.object.team, function(element, index, list){
				$('ul[dataObjectTeam]').append('<li>'+element.post+': <span>'+element.fio+'</span></li>');
			});
			$('ul[dataObjectTags]').html( '' );
			_.each(data.data.object.tags, function(element, index, list){
				$('ul[dataObjectTags]').append('<li class="'+element.alias+'">'+element.name+'</li>');
			});
			$('div[dataObjectShortDescription]').html( data.data.object.short_description );
			$('article > .visitSite').attr("href",data.data.object.url);
}
function buttonMoreOnclick(workName, dataArticle){
	parallax.show.querySelector('.work_' + location.hash.slice(6)).classList.add('full');
	parallax.container.classList.add('hidelinks');
	parallax.moveable.style.top = '-345.885%';
	parallax.moveable.style.left = '-411.77%';
	$.when( stepCaseOne(workName, dataArticle) ).then( function( data, dataArticle ) { stepCaseTo(data, dataArticle); } );
}
